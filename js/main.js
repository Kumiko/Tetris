
/*メモ：回転と移動を(ほぼ)同時に行うとバグる*/
document.addEventListener("keydown", onKeyDown);

var score = 0;
var cells;
var isFalling = false;
var fallingBlockNum = 0;
var gameOver = false;	//ゲームオーバー判定
var gameStart = false;//ゲーム開始判定(余剰かも)
var blockPattern;			//ブロックパターン(回転用)
var blockPosRow = 20;	//ブロックを構成する長方形の左上の座標
var blockPosCol = 10; //ブロックを構成する長方形の左上の座標
var checkPos = false; //blockPosRow, blockPosColが取得できたか


var blocks = {
	i: {
		class: "i",
		pattern: [[1, 1, 1 ,1]]
	},
	o: {
		class: "o",
		pattern: [[1, 1],[1 ,1]]
	},
	t: {
		class:"t",
		pattern:[[0, 1, 0],[1, 1 ,1]]
	},
	s: {
		class:"s",
		pattern:[[0, 1, 1],[1 ,1, 0]]
	},
	z: {
		class:"z",
		pattern:[[1, 1, 0],[0, 1 ,1]]
	},
	j: {
		class:"j",
		pattern:[[1, 0, 0],[1, 1 ,1]]
	},
	l: {
		class:"l",
		pattern:[[0, 0, 1],[1, 1 ,1]]
	}
};

loadTable();
setInterval(function(){
	document.getElementById("hello_text").textContent = "Push Enter to Start!";
	if(gameOver === true){
		alert("game over");
		loadTable();
		isFalling = false;
		gameStart = false;
	}
	if(gameStart === true){
		document.getElementById("hello_text").textContent = "SCORE(" + score + ")";
		if(hasFallingBlock()){
			fallBlocks();
		}else{
			deleteRow();
			generateBlock();
		}
	}
},500);

function loadTable(){
	cells = [];
	score = 0;
	var td_array = document.getElementsByTagName("td");
	var index = 0;
	for(var row = 0; row < 20; row++){
		cells[row] = [];
		for(var col = 0; col < 10; col++){
			cells[row][col]= td_array[index];
			cells[row][col].className = "";
			cells[row][col].blockNum = null;
			index++;
		}
	}
	gameOver = false;
}

function fallBlocks(){
	var rowNum = 0;
	var colNum = 0;
	checkPos = false;

	for(var col = 0; col < 10; col++){
		if(cells[19][col].blockNum === fallingBlockNum){
			isFalling = false;
			return;
		}
	}

	/*下との衝突判定*/
	for(var row = 18; row >= 0; row--){
		for(var col = 0; col < 10; col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row+1][col].className != "" && cells[row+1][col].blockNum !== fallingBlockNum){
					isFalling = false;
					isGameOver();
					return;
				}
			}
		}
	}

	for(var row = 18; row >= 0; row--){
		for(var col = 0; col < 10; col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				cells[row+1][col].className = cells[row][col].className;
				cells[row+1][col].blockNum = cells[row][col].blockNum;
				cells[row][col].className = "";
				cells[row][col].blockNum = null;
				//ブロックの座標取得
				blockPosRow = row+1;
				if(!checkPos && col < blockPosCol){
					blockPosCol = col;
				}
			}
		}
	}
	checkPos = true;
}

function hasFallingBlock(){
	return isFalling;
}

function deleteRow(){
	var countRow = 0;
	for (var row = 19; row >= 0; row--) {
    var canDelete = true;
    for (var col = 0; col < 10; col++) {
      if (cells[row+countRow][col].className === "") {
        canDelete = false;
      }
    }
    if (canDelete) {
			score += 100;
      for (var col = 0; col < 10; col++) {
        cells[row+countRow][col].className = "";
      }
      for (var downRow = row+countRow - 1; downRow >= 0; downRow--) {
        for (var col = 0; col < 10; col++) {
          cells[downRow + 1][col].className = cells[downRow][col].className;
          cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
          cells[downRow][col].className = "";
          cells[downRow][col].blockNum = null;
        }
      }
			countRow++;
    }
  }
}

function generateBlock(){
	var keys = Object.keys(blocks);
	var nextBlockKey = keys[Math.floor(Math.random()*keys.length)];
	var nextBlock = blocks[nextBlockKey];
	var nextFallingBlockNum = fallingBlockNum + 1;
	var pattern = nextBlock.pattern;
	blockPattern = pattern;	//ブロックパターンのコピー
	blockPosRow = 20;				//テーブル外の値で初期化
	blockPosCol = 10;				//テーブル外の値で初期化

	for(var row = 0; row < pattern.length; row++){
		for(var col = 0; col < pattern[row].length; col++){
			if(pattern[row][col]){
				cells[row][col+3].className = nextBlock.class;
				cells[row][col+3].blockNum = nextFallingBlockNum;
			}
		}
	}
	isFalling = true;
	checkPos = false;
	fallingBlockNum = nextFallingBlockNum;
}

/*ゲームオーバー判定*/
function isGameOver(){
	for(var col = 3; col < 7; col++){
		if(cells[0][col].className !== ""){
			gameOver = true;
			return;
		}
	}
}

/*操作関連*/
function onKeyDown(event){
	if(event.keyCode === 13){
		gameStart = true;
	}
	if(isFalling){
		if(event.keyCode === 37){
			checkPos = false;
			moveLeft();
		}else if(event.keyCode === 39){
			checkPos = false;
			moveRight();
		}else if(event.keyCode === 82){//Rで回転
			rotateBlock();
		}
	}
}

function moveLeft(){
	//左が壁か
	for(var row = 0; row < 20; row++){
		if(cells[row][0].blockNum === fallingBlockNum){
			return;
		}
		//左に他のブロックがあるか
		for(var col = 0; col < 10; col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row][col-1].className != "" && cells[row][col-1].blockNum !== fallingBlockNum){
					return;
				}
			}
		}
	}

	for(var row = 0; row < 20; row++){
		for(var col = 0; col < 10; col++){
			if(cells[row][col].blockNum === fallingBlockNum){
				cells[row][col-1].className = cells[row][col].className;
				cells[row][col-1].blockNum = cells[row][col].blockNum;
				cells[row][col].className = "";
				cells[row][col].blockNum = null;
			}
		}
	}
}

function moveRight(){
	//右が壁か
	for(var row = 0; row < 20; row++){
		if(cells[row][9].blockNum === fallingBlockNum){
			return;
		}
		//右に他のブロックがあるか
		for(var col = 9; col >= 0; col--){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row][col+1].className != "" && cells[row][col+1].blockNum !== fallingBlockNum){
					return;
				}
			}
		}
		blockPosCol +=1;
	}

	for(var row = 0; row < 20; row++){
		for(var col = 9; col >= 0; col--){
			if(cells[row][col].blockNum === fallingBlockNum){
				if(cells[row][col+1].className != "" && cells[row][col+1].blockNum !== fallingBlockNum){
					return;
				}
				cells[row][col+1].className = cells[row][col].className;
				cells[row][col+1].blockNum = cells[row][col].blockNum;
				cells[row][col].className = "";
				cells[row][col].blockNum = null;
			}
		}
	}
}

/*右回転*/
function rotateBlock(){
	var rotated;
	//回転後のパターン保持
	rotated = new Array(blockPattern[0].length);
	for(var row = 0; row < blockPattern[0].length; row++){
		rotated[row] = new Array(blockPattern.length);
		for(var col = 0; col < blockPattern.length; col++){
			rotated[row][col] = blockPattern[blockPattern.length - 1 - col][row];
		}
	}

	//回転後の衝突予測
	for(var row = 0; row <rotated.length; row++){
		for(var col = 0; col < rotated[0].length; col++){
			if(cells[blockPosRow+row][blockPosCol+col].className != "" && cells[blockPosRow+row][blockPosCol+col].blockNum !== fallingBlockNum){
				return;
			}
		}
	}

	//元のブロック描画箇所を初期化
	for(var row = 0; row <rotated[0].length; row++){
		for(var col = 0; col < rotated.length; col++){
			if(cells[blockPosRow+row][blockPosCol+col].blockNum === fallingBlockNum){
				var className = cells[blockPosRow+row][blockPosCol+col].className;
				var blockNum = cells[blockPosRow+row][blockPosCol+col].blockNum;
	//		}
				cells[blockPosRow+row][blockPosCol+col].className = "";
				cells[blockPosRow+row][blockPosCol+col].blockNum = null;
			}
		}
	}

	//回転後のブロック描画
	for(var row = 0; row <rotated.length; row++){
		for(var col = 0; col < rotated[0].length; col++){
			if(rotated[row][col] === 1){
				cells[blockPosRow+row][blockPosCol+col].className = className;
				cells[blockPosRow+row][blockPosCol+col].blockNum = blockNum;
			}
		}
	}
	blockPattern = rotated;
}
